﻿import { Observable } from 'rxjs';

export interface GetNumberResponse {
  number: number;
}
export interface ProtoService {
  getNumber({}): Observable<GetNumberResponse>;
}

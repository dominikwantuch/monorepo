import { Module } from '@nestjs/common';
import { GrpcClientController } from './grpc-client.controller';
import { join } from 'path';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [ClientsModule.register([
    {
      name: 'PROTO_SERVICE',
      transport: Transport.GRPC,
      options: {
        package: 'ProtoService',
        protoPath: join(
          __dirname,
          'proto-service.proto')
      }
    }
  ])],
  controllers: [GrpcClientController],
  providers: []
})
export class GrpcClientModule {
}

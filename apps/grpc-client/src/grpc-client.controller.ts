import { Controller, Get, Inject } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { ProtoService } from './proto-service';
import { firstValueFrom } from 'rxjs';

@Controller()
export class GrpcClientController {
  private readonly protoService: ProtoService;

  constructor(@Inject('PROTO_SERVICE') private client: ClientGrpc) {
    this.protoService = client.getService<ProtoService>('ProtoService');
  }

  @Get()
  async test(): Promise<number> {
    const res = await firstValueFrom(this.protoService.getNumber({}));
    return res.number;
  }
}

import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { GrpcMethod } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @GrpcMethod('ProtoService', 'GetNumber')
  getNumber({}): any {
    return { number: Math.round(Math.random() * 10) };
  }
}
